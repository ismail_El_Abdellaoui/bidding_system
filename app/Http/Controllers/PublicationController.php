<?php

namespace App\Http\Controllers;

use App\Events\AdCreateEvent;
use App\Models\Publication;
use Illuminate\Http\Request;
use App\Rules\budgetValidator;
use Illuminate\Support\Facades\View;

class PublicationController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $min=500;
        $validation = $request->validate([
            'nom'=>'required|min:3',
            'region'=>'required|min:3',
            'audience'=>'required',
            'budget'=>['required', "gt:$min", new budgetValidator],
            'video'=>'required'
        ]) ;


        $pub = new Publication();
        $pub->nom = $request->nom;
        $pub->region = $request->region;
        $pub->audience = $request->audience;
        $pub->budget = ($request->budgetInitial*$request->CPM)*1000;
        if ($request->hasFile('video')) {

            $pub->video = $request->file('video')->store('videos' , 'public');
        }else{
            $pub->video = "no vid";
        }

        $pub->save();

        broadcast( new AdCreateEvent($pub->budget) );
        return [
            'msg' => 'i did it'
        ];
    }
    
    public function getHighest()
    {
        return Publication::orderBy('budget','desc')->first();
    }

}
