<?php

namespace App\Rules;

use App\Models\Publication;
use Illuminate\Contracts\Validation\Rule;

class budgetValidator implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return Publication::where('budget','>' , $value )->count() == 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The budget is lower than the highest.';
    }
}
