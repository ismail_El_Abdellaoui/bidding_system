<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <style>
            .feedback-walidation {
                width: 100%;
                margin-top: .25rem;
                font-size: .875em;
                color: #dc3545;
            }
        </style>
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    </head>
    <body class="antialiased">

        <div id="app">
            <div class="container">

                @if($errors->any())
                    <new-publicite :errors="{{ $errors->all() }}"></new-publicite>
                @else
                    <new-publicite></new-publicite>
                @endif

            </div>
        </div>
        <script src="{{ asset('js/app.js') }}" defer></script>
    </body>
</html>
