<?php

use App\Http\Controllers\PublicationController;
use App\Events\AdCreateEvent;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/msg', function () {
    broadcast( new AdCreateEvent() );
});

Auth::routes();

Route::resource('publication',PublicationController::class)->only(['store']);

Route::get('/highest', [App\Http\Controllers\PublicationController::class, 'getHighest'])->name('highest');
